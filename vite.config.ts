import { sentrySvelteKit } from '@sentry/sveltekit';
import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [
		sentrySvelteKit({
			sourceMapsUploadOptions: {
				project: 'aedopte',
				release: {
					inject: true,
					name: process.env.TAG,
				},
				unstable_sentryVitePluginOptions: {
					release: {
						setCommits: {
							auto: true,
                            ignoreMissing: true,
						},
					},
				},

			}
		}),

		sveltekit()
	],
	resolve: {
		alias: {
			'.prisma/client/index-browser': './node_modules/.prisma/client/index-browser.js'
		}
	}
});
