import { Prisma } from '@prisma/client';
import { Major } from '@prisma/client';

export let colorsList = [
	{
		text: 'black',
		bg: '#FEC6A9'
	}
];

const userWithGroups = Prisma.validator<Prisma.UserDefaultArgs>()({
	include: {
		groups: {
			include: {
				group: true
			}
		}
	}
});

export type UserWithGroups = Prisma.UserGetPayload<typeof userWithGroups>;

export async function fetchChurrosForGroups() {
	return await fetch('https://churros.inpt.fr/graphql', {
		method: 'POST',
		body: JSON.stringify({
			query: `
                 query{
                    groups {
                        pictureURL
                        name
                    }
                 }
                 `
		})
	}).then(async (r) => {
		const result = await r.json();
		return result;
	});
}

export function majorToText(major: Major) {
	if (major === 'sdn') {
		return 'SN';
	} else if (major === 'eeea') {
		return '3EA';
	} else {
		return 'MF2E';
	}
}
