import { prisma } from '$lib/server/prisma';

export async function executeMariageAlgorithm() {
	let godChildIdList : string[] = [];

	const godparents = await prisma.user.findMany({
		where: {
			yearTier: 2,
			alreadyFindMate: false
		},
		include: {
			childUsers: {
				where: {
					relation: 'LIKE'
				},
				select: {
					rank: true,

					childUser: {
						include: {
							groups: true
						}
					}
				}
			},
			groups: true
		}
	});

	const godchildren = await prisma.user.findMany({
		where: {
			yearTier: 1,
			alreadyFindMate: false
		},
		include: {
			groups: true
		}
	});

	// Sum of the factors must be 100
	const RANK_FACTOR = [200, 120, 60, 20, 10]
	const CLUB_FACTOR = 100; // Facteur des clubs
	const MAJOR_FACTOR = 0; // Facteur de la filière
	const SAME_PATH_FACTOR = 50; // Facteur du même parcours (FISE ou FISA)

	let numberPositionRankingMatch = [0, 0, 0, 0, 0];

	const godparentToGodchildrenScore = new Map<
		string,
		{ godparentId: string; godchildren: string; score: number }[]
	>();

	console.info(godparents.length);

	godparents.forEach((godparent) => {
		for (const children of godparent.childUsers) {
			const childrenInfo = children.childUser;

			//N'est pas censé arriver, surement supprimable
			if (!children) {
				console.warn(`Children not found`);
				continue;
			}

			let sharedClubs =
				childrenInfo.groups.reduce((acc, e) => (godparent.groups.includes(e) ? 1 + acc : acc), 0) /
				godparent.groups.length;
			if (isNaN(sharedClubs)) {
				sharedClubs = 0;
			}

			const score =
				RANK_FACTOR[children.rank! - 1]+
				CLUB_FACTOR * sharedClubs +
				MAJOR_FACTOR * (godparent.major === childrenInfo.major ? 1 : 0) +
				SAME_PATH_FACTOR * (godparent.apprentice === childrenInfo.apprentice ? 1 : 0) +
				Math.random() * 2; // Random factor to avoid same factor

			console.info(
				`Godparent ${godparent.givenName} has a score of ${score} with godchild ${childrenInfo.givenName}`
			);

			// Add to the map
			if (!godparentToGodchildrenScore.has(godparent.id)) {
				godparentToGodchildrenScore.set(godparent.id, []);
			}
			godparentToGodchildrenScore
				.get(godparent.id)!
				.push({ godparentId: godparent.id, godchildren: childrenInfo.id, score });
		}
	});

	// Sort by score
	const godparentToGodchildrenLeaderboard = Array.from(godparentToGodchildrenScore.values())
		.flat()
		.sort((a, b) => b.score - a.score);

	// Final list
	const godparentToGodchildren = new Map<string, string>();

	// While the list is not empty
	// 1. Get all the godchildren of the godparent with the highest score
	// 2. Check if there is the same godparent in the list
	// 3. Check if there is the same godchild in the list
	// 4. If not, add the godparent and the godchild to the list
	// 5. Remove the godparent and the godchild from the list

	while (godparentToGodchildrenLeaderboard.length > 0) {
		// Get the godparent with the highest score
		const sameScore = [godparentToGodchildrenLeaderboard.shift()!];

		// Get all godparentToGodchildren with the same score and remove them from the list
		for (let i = 0; i < godparentToGodchildrenLeaderboard.length; i++) {
			if (godparentToGodchildrenLeaderboard[i].score === sameScore[0].score) {
				sameScore.push(godparentToGodchildrenLeaderboard.splice(i--, 1)[0]);
			}
		}

		console.info(
			`Godparents with the score ${sameScore[0].score}: ${sameScore.map((e) => e.godparentId).join(', ')}`
		);

		// Check if there is no duplicate godchildren
		const DiplicatedgodchildrenSet = new Set<string>();
		for (const e of sameScore) {
			if (DiplicatedgodchildrenSet.has(e.godchildren)) {
				console.warn('Duplicate godchildren found');
				continue;
			}
			DiplicatedgodchildrenSet.add(e.godchildren);
		}
		// Choose a random godchild and remove the others from sameScore
		const godchild = sameScore[Math.floor(Math.random() * sameScore.length)].godchildren;
		for (let i = 0; i < sameScore.length; i++) {
			if (sameScore[i].godchildren !== godchild) {
				sameScore.splice(i--, 1);
			}
		}

		// Check if there is no duplicate godparents
		const DiplicatedgodparentSet = new Set<string>();
		for (const e of sameScore) {
			if (DiplicatedgodparentSet.has(e.godparentId)) {
				console.warn('Duplicate godparents found');
				continue;
			}
			DiplicatedgodparentSet.add(e.godparentId);
		}
		// Choose a random godparent and remove the others from sameScore
		if(sameScore[0].score > 70 ) {
			const godparent = sameScore[Math.floor(Math.random() * sameScore.length)].godparentId;
			for (let i = 0; i < sameScore.length; i++) {
				if (sameScore[i].godparentId !== godparent) {
					sameScore.splice(i--, 1);
				}
			}

			// Add to the final list and remove from the leaderboard all the godparentToGodchildren with the same godparent or godchild
			for (const e of sameScore) {
				const childrenMateID = await prisma.user.findUnique({
					where: {
						id: e.godchildren
					},
					select: {
						mateId: true
					}
				});

				//Hotfix pitié
				if (!childrenMateID?.mateId && !godparentToGodchildren.has(e.godparentId)) {
					godparentToGodchildren.set(e.godparentId, e.godchildren);
					console.info(`Godparent ${e.godparentId} is now married to godchild ${e.godchildren}`);

					for (let i = 0; i < godparentToGodchildrenLeaderboard.length; i++) {
						if (
							godparentToGodchildrenLeaderboard[i].godparentId === e.godparentId ||
							godparentToGodchildrenLeaderboard[i].godchildren === e.godchildren
						) {
							godparentToGodchildrenLeaderboard.splice(i--, 1);
						}
					}

					// Update the database with the new couple of godparent and godchild
					await prisma.user.update({
						where: {
							id: e.godparentId
						},
						data: {
							alreadyFindMate: true,
							mateId: e.godchildren
						}
					});

					await prisma.user.update({
						where: {
							id: e.godchildren
						},
						data: {
							alreadyFindMate: true,
							mateId: e.godparentId
						}
					});

					const rankShow = await prisma.linkedUsers.findUnique({ 
						where: {
							parentUserId_childUserId: {
								childUserId: e.godchildren,
								parentUserId: e.godparentId
							}
						},
						select: {
							rank: true
						}
					});
				
					console.info(`Rank of the relation between ${e.godchildren} child and ${e.godparentId} : `, rankShow?.rank);

					numberPositionRankingMatch[rankShow?.rank! - 1] = numberPositionRankingMatch[rankShow?.rank! - 1] + 1;

					// Update the rank of the godparent's godchildren
					// Get all the godparent's of the new affected godchildren that cannot have a new godparent
					const relations = await prisma.linkedUsers.findMany({
						where: {
							childUserId: e.godchildren,
							relation: 'LIKE'
						},
						select: {
							parentUserId: true,
							rank: true
						}
					});

					for (const relation of relations) {
						// Get all relation that need to be updated
						const godParentRelations = await prisma.linkedUsers.findMany({
							where: {
								parentUserId: relation.parentUserId,
								relation: 'LIKE',
							}
						});

						const numberRelations = godParentRelations.length;
						for (let i = 0; i < numberRelations; i++) {
							const needUpdateRelation = await prisma.linkedUsers.findFirst({
								where: {
									childUserId: godParentRelations[i].childUserId,
									parentUserId: relation.parentUserId,
									relation: 'LIKE',
									childUser: {
										alreadyFindMate: false
									}
								},
								select: {
									rank: true
								}
							});

							// If the rank is higher than the rank of the new godchild, decrement the rank
							if (needUpdateRelation?.rank! > relation.rank!) {
								await prisma.linkedUsers.update({
									where: {
										parentUserId_childUserId: {
											childUserId: godParentRelations[i].childUserId,
											parentUserId: relation.parentUserId
										}
									},
									data: {
										rank: {
											decrement: 1
										}
									}
								});
							}
						}
						//Actualize relation in the database, supress relations of user that find a mate.
						/*await prisma.linkedUsers.deleteMany({
							where: {
								OR: [
									{
										parentUserId: e.godparentId
									},
									{
										childUserId: e.godchildren
									}
								],
								relation: 'LIKE'
							}
						}); */
					}
				}
			}
		}
	}



	console.info('Number of rank match : ', numberPositionRankingMatch);

}
