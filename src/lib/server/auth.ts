import { Lucia } from 'lucia';
import { PrismaAdapter } from '@lucia-auth/adapter-prisma';
import { prisma } from './prisma';
import { Authentik } from 'arctic';
import { env } from '$env/dynamic/private';

const adapter = new PrismaAdapter(prisma.session, prisma.user);

const realmUrl = 'https://auth.inpt.fr';

export const authentikOauth = new Authentik(
	realmUrl,
	env.AUTHENTIK_CLIENT_ID,
	env.AUTHENTIK_SECRET_CLIENT_ID,
	env.REDIRECT_URL
);

export const lucia = new Lucia(adapter, {
	getUserAttributes: (attributes) => {
		return {
			churrosUid: attributes.churrosUid,
			givenName: attributes.givenName,
			admin: attributes.admin // A voir avec Evann....
		};
	},
	getSessionAttributes: (attributes) => {
		return {
			userId: attributes.userId,
			expiresAt: attributes.expiresAt
		};
	},
	sessionCookie: {
		attributes: {
			// set to `true` when using HTTPS
			secure: process.env.NODE_ENV === 'production' ? false : true
		}
	}
});

declare module 'lucia' {
	interface Register {
		Lucia: typeof lucia;
		DatabaseSessionAttributes: DatabaseSessionAttributes;
		DatabaseUserAttributes: DatabaseUserAttributes;
	}
}

interface DatabaseUserAttributes {
	churrosUid: string;
	givenName: string;
	admin: boolean;
}

interface DatabaseSessionAttributes {
	userId: string;
	expiresAt: Date;
}
