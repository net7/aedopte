import { writable } from "svelte/store";
import { browser } from "$app/environment";

const defaultShowFirstModal = 'true';
let storedShowFirstModal = browser && localStorage.getItem('showFirstModal') || defaultShowFirstModal;
export const showFirstModal = writable(storedShowFirstModal);
showFirstModal.subscribe((val) => browser && localStorage.setItem('showFirstModal', val));
