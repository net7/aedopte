import { redirect } from '@sveltejs/kit';
import { prisma } from '$lib/server/prisma';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals }) => {
	const { user } = locals;

	// Page réservée aux utilisateurs en 2eme année
	if (!user) {
		redirect(302, '/login');
	} else if (user.yearTier !== 2) {
		redirect(302, '/registration');
	}

	// Si utilisateur en 2eme année, on fetch la db en quete des utilisateur qu'il a like
	if (user.yearTier === 2) {
		const maybeSelectedUsersId = await prisma.linkedUsers.findMany({
			where: {
				parentUserId: user.id,
				relation: 'MAYBE'
			},
			select: {
				rank: true,
				childUser: {
					select: {
						id: true
					}
				}
			}
		});

		const maybeUsersId = maybeSelectedUsersId.map((e) => {
			return e.childUser.id;
		});

		const maybeUsersWithGroups = await prisma.user.findMany({
			where: {
				id: {
					in: maybeUsersId
				},
				yearTier: 1
			},
			include: {
				groups: {
					where: {
						group: {
							visible: true
						}
					},
					include: {
						group: true
					}
				}
			}
		});

		const likedUsersNumber = await prisma.linkedUsers.count({
			where: {
				parentUserId: user.id,
				relation: 'LIKE'
			}
		});

		const mate = await prisma.user.findUnique({
			where: {
				mateId: user.id
			}
		});

		const settings = await prisma.settings.findMany();

		return {
			me: locals.user,
			settings: settings,
			maybeUsersWithGroups,
			likedUsersNumber,
			mate
		};
	} else {
		redirect(302, '/registration');
	}
};
