import { redirect } from '@sveltejs/kit';
import { prisma } from '$lib/server/prisma';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals }) => {
	const { user } = locals;

	// Page réservée aux utilisateurs en 2eme année
	if (!user) {
		redirect(302, '/login');
	} else if (user.yearTier !== 2) {
		redirect(302, '/registration');
	}

	const likedUsersNumber = await prisma.linkedUsers.count({
		where: {
			parentUserId: user.id,
			relation: 'LIKE'
		}
	});

	const dislikedUsersNumber = await prisma.linkedUsers.count({
		where: {
			parentUserId: user.id,
			relation: 'DISLIKE'
		}
	});

	const maybeUsersNumber = await prisma.linkedUsers.count({
		where: {
			parentUserId: user.id,
			relation: 'MAYBE'
		}
	});

	const mate = await prisma.user.findUnique({
		where: {
			mateId: user.id
		}
	});

	const settings = await prisma.settings.findMany();

	return {
		settings: settings,
		likedUsersNumber,
		dislikedUsersNumber,
		maybeUsersNumber,
		mate
	};
};
