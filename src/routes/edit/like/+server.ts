import { json } from '@sveltejs/kit';
import { prisma } from '$lib/server/prisma';

import { z } from 'zod';

const groupSchema = z.object({
	id: z.string(),
	uid: z.string(),
	name: z.string(),
	pictureURL: z.string().nullable(),
	type: z.enum(['Club', 'Association', 'StudentAssociationSection'])
});

const groupObjectSchema = z.object({
	groupId: z.string(),
	userId: z.string(),
	group: groupSchema
});

// Définition du schéma Zod pour le modèle User
const UserSchema = z.object({
	id: z.string(), // Prisma génère une valeur par défaut pour l'ID
	churrosUid: z.string(), // Champ unique, donc on peut supposer qu'il est requis
	profilePicURL: z.string().nullable(), // Optionnel et doit être une URL valide si fourni
	givenName: z.string().default(''), // Valeur par défaut si non fourni
	surname: z.string().nullable(),
	description: z.string().nullable(),
	hobbies: z.string().nullable(),
	admin: z.boolean().default(false), // Valeur par défaut
	yearTier: z.number().gt(0).lt(2), // Doit être un entier entre 1 et 2
	apprentice: z.boolean().default(false), // Valeur par défaut
	major: z.enum(['sdn', 'eeea', 'mfee']),
	alreadyFindMate: z.boolean().default(false), // Valeur par défaut
	groups: z.array(groupObjectSchema).nullable() // Possible que le 1A ai mis aucun groupe
});

const requestBodySchema = z.object({
	user: UserSchema,
	belowUsersIds: z.array(z.string()),
	relationType: z.enum(['LIKE', 'DISLIKE', 'MAYBE'])
});

export async function POST({ request, locals }) {
	try {
		const data = await request.json();
		const relationData = requestBodySchema.parse(data);

		if (locals.user != null) {
			await prisma.linkedUsers.update({
				data: {
					childUserId: relationData.user.id,
					parentUserId: locals.user.id,
					relation: relationData.relationType,
					rank: null
				},
				where: {
					parentUserId_childUserId: {
						childUserId: relationData.user.id,
						parentUserId: locals.user.id
					}
				}
			});

			for (var id of relationData.belowUsersIds) {
				await prisma.linkedUsers.update({
					data: {
						rank: {
							decrement: 1
						}
					},
					where: {
						parentUserId_childUserId: {
							childUserId: id,
							parentUserId: locals.user.id
						}
					}
				});
			}
		}

		return json({ success: true });
	} catch (error) {
		return json({ 
			success: false,
			error
		});
	}

}
