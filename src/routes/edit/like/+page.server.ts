import { redirect } from '@sveltejs/kit';
import { prisma } from '$lib/server/prisma';
import type { PageServerLoad } from './$types';
import { majorToText } from '$lib';
export const load: PageServerLoad = async ({ locals }) => {
	const { user } = locals;

	// Page réservée aux utilisateurs en 2eme année
	if (!user) {
		redirect(302, '/login');
	} else if (user.yearTier !== 2) {
		redirect(302, '/registration');
	}

	// Si utilisateur en 2eme année, on fetch la db en quete des utilisateur qu'il a like
	if (user.yearTier === 2) {
		const likedSelectedUsersId = await prisma.linkedUsers.findMany({
			where: {
				parentUserId: user.id,
				relation: 'LIKE'
			},
			select: {
				rank: true,
				childUser: {
					select: {
						id: true
					}
				}
			}
		});

		const likedUsersId = likedSelectedUsersId.map((e) => {
			return e.childUser.id;
		});

		const likedUsersWithGroups = await prisma.user.findMany({
			where: {
				id: {
					in: likedUsersId
				},
				yearTier: 1
			},
			include: {
				groups: {
					where: {
						group: {
							visible: true
						}
					},
					include: {
						group: true
					}
				}
			}
		});

		const likedUsersItems: {
			id: string;
			rank: number;
			rankedUserName: string;
			majorText: string;
		}[] = [];

		for (let i = 0; i < likedUsersId.length; i++) {
			const elt = likedUsersWithGroups[i];
			const rank = likedSelectedUsersId.filter((obj) => {
				return obj.childUser.id === elt.id;
			})[0].rank;
			likedUsersItems[i] = {
				id: elt.id,
				rank: rank ?? i + 1,
				rankedUserName: elt.givenName,
				majorText: majorToText(elt.major)
			};
		}

		likedUsersItems.sort((a, b) => (a.rank > b.rank ? 1 : -1));

		const mate = await prisma.user.findUnique({
			where: {
				mateId: user.id
			}
		});

		const settings = await prisma.settings.findMany();

		return {
			me: locals.user,
			settings: settings,
			likedUsersWithGroups,
			likedUsersItems,
			likedUsersId,
			mate
		};
	} else {
		redirect(302, '/registration');
	}
};
