import { json } from '@sveltejs/kit';
import { prisma } from '$lib/server/prisma';

import { z } from 'zod';

// Définition du schéma Zod pour le modèle UserItem
const UserItems = z.object({
	id: z.string(),
	rank: z.number(),
	rankedUserName: z.string(),
	majorText: z.string()
});

const requestBodySchema = z.object({
	likedUserItemsInOrder: z.array(UserItems)
});

export async function POST({ request, locals }) {
	const data = await request.json();
	const relationData = requestBodySchema.parse(data);

	if (locals.user != null) {
		for (var item of relationData.likedUserItemsInOrder) {
			await prisma.linkedUsers.update({
				data: {
					rank: item.rank
				},
				where: {
					parentUserId_childUserId: {
						childUserId: item.id,
						parentUserId: locals.user.id
					}
				}
			});
		}
	}

	return json({ success: true });
}
