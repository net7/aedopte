import type { PageServerLoad } from './$types';
import { redirect } from '@sveltejs/kit';
import { prisma } from '$lib/server/prisma';

export const load: PageServerLoad = async ({ locals }) => {
	const { user } = locals;

	if (!user) {
		redirect(302, '/login');
	}

	// Recup les id des 1A déjà vu
	const viewedUsersId = await prisma.user
		.findFirst({
			where: {
				id: user.id
			},
			select: {
				childUsers: true
			}
		})
		.then((user) => {
			return user?.childUsers.map((user) => user.childUserId);
		});

	const likedUsersNumber = await prisma.linkedUsers.count({
		where: {
			parentUserId: user.id,
			relation: 'LIKE'
		}
	});

	//Les users qui n'ont pas encore été noté par le 2A
	const neverRatedUsers = await prisma.user.findMany({
		where: {
			id: {
				notIn: viewedUsersId
			},
			yearTier: 1,
			alreadyFindMate: false,
			major: user.major // Pour être certain d'avoir des gens de la même filière
		},
		include: {
			groups: {
				where: {
					group: {
						visible: true
					}
				},
				include: {
					group: true
				}
			}
		}
	});

	const mate = await prisma.user.findUnique({
		where: {
			mateId: user.id
		}
	});

	const mateWithGroups = await prisma.user.findUnique({
		where: {
			mateId: user.id
		},
		include: {
			groups: {
				where: {
					group: {
						visible: true
					}
				},
				include: {
					group: true
				}
			}
		}
	});

	const groups = await prisma.group.findMany({
		where: {
			visible: true
		}
	});

	const settings = await prisma.settings.findMany();

	return {
		me: locals.user,
		neverRatedUsers,
		likedUsersNumber,
		mate,
		mateWithGroups,
		groups,
		settings
	};
};
