import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { prisma } from '$lib';
import { executeMariageAlgorithm } from '$lib/server/mariageStable';
import type { Group } from '@prisma/client';

export const actions = {
	hideClubs: async ({ request }) => {
		const data = await request.formData();

		// On récupère les groupes
		const groups = await prisma.group.findMany({});

		// Remet a jour la visibilité des groupes
		for (let group of groups) {
			const groupString = data.get(group.uid) as string;
			if (groupString) {
				//C'est une catastrophe, mais en gros premier elem c'est un id, le deuxieme c'est show ou hide
				const groupInfo = groupString.split(', ');
				await prisma.group.update({
					where: {
						id: groupInfo[0]
					},
					data: {
						visible: groupInfo[1] === 'show' ? true : false
					}
				});

				// Possible que je tej ça, dans l'idée c'est pour retrer des possible lien non voulu
				if (groupInfo[1] === 'hide') {
					await prisma.groupMember.deleteMany({
						where: {
							groupId: groupInfo[0]
						}
					});
				}
			}
		}
	},

	matchAlgorithm: async ({ request }) => {
		console.info("lancement de l'algo de mariage");
		await executeMariageAlgorithm();
		console.info("fin de l'algo de mariage");
	},

	resetAlgorithm: async ({ request }) => {
		console.info("reset de l'algo de match");
		await prisma.user.updateMany({
			data: {
				// nécessite des correction, ne suppr pas le champ godParent
				mateId: null,
				alreadyFindMate: false
			}
		});
	},

	showHints: async ({ request }) => {
		console.info('changement du setting showHint');

		const settingShowHint = await prisma.settings.findUnique({
			where: {
				key: 'showHint'
			}
		});

		let showHint = settingShowHint?.value === 'true' ? 'false' : 'true';

		await prisma.settings.update({
			where: {
				key: 'showHint'
			},
			data: {
				value: showHint
			}
		});
		redirect(302, '/adminboard');
	},

	fixRankRelation: async ({ request }) => {
		console.info("Fix relation rank in db");

		try {
			// Supression des gens qui ont déjà un mate GHETTO DE FOU HOTFIX DE BZ
			await prisma.linkedUsers.deleteMany({ 
				where: {
					OR: [
						{
							childUser: {
								alreadyFindMate: true
							}
						},
						{
							parentUser: {
								alreadyFindMate: true
							}
						}
					]
					
				}
			})

			const secondYearUsers = await prisma.user.findMany({
				where: {
					yearTier: 2
				},
				include: {
					childUsers: {
						where: {
							relation: 'LIKE'
						},
						orderBy: {
							rank: "asc"
						},
					}
				}
			});

			for(const secondYearUser of secondYearUsers) {
				
				for(let i = 0; i < secondYearUser.childUsers.length; i++){
					await prisma.linkedUsers.update({
						where:{
							parentUserId_childUserId: {
								parentUserId: secondYearUser.id,
								childUserId: secondYearUser.childUsers[i]?.childUserId
							},
						},
						data: {
							rank: i+1
						}
					})
				}
			}

		} catch {
			console.info("Erreur dans la tentative de fixer le problème des rank") 
		}
	},

	populateGroups: async ({ request }) => {
		console.info('populate groups in db');

		// init groups from churros
		let groupsResquet = await fetch('https://churros.inpt.fr/graphql', {
			method: 'POST',
			mode: 'cors',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				query: `
                    {
                        groups {
                            name
                            uid
                            pictureURL
                            type
                        }
                    }
                 `
			})
		}).then(async (r) => {
			const result = await r.json();
			return result;
		});

		let groups: Group[] = groupsResquet.data.groups;

		//Filtre afin de ne selectionner que les clubs, asso et AE.
		const selectedGroupType = ['Club', 'Association', 'StudentAssociationSection'];
		groups = groups.filter((group) => selectedGroupType.includes(group.type));

		await prisma.group.deleteMany({});
		await prisma.group.createMany({ data: groups });
	}


};

export const load: PageServerLoad = async ({ locals }) => {
	const { user } = locals;
	// Vérification des droits admins
	if (!user || !user.admin) {
		redirect(302, '/');
	}

	const groups = await prisma.group.findMany({});

	const aloneSecondYearUser = await prisma.user.findMany({
		where: {
			yearTier: 2,
			alreadyFindMate: false
		},
		include: {
			groups: {
				where: {
					group: {
						visible: true
					}
				},
				include: {
					group: true
				}
			}
		},
		orderBy: {
			givenName: 'asc'
		}
	});

	const aloneFirstYearUser = await prisma.user.findMany({
		where: {
			yearTier: 1,
			alreadyFindMate: false
		},
		include: {
			groups: {
				where: {
					group: {
						visible: true
					}
				},
				include: {
					group: true
				}
			}
		},
		orderBy: {
			givenName: 'asc'
		}
	});

	const relations = await prisma.user.findMany({
		where: {
			yearTier: 1,
			alreadyFindMate: true,
			NOT: {
				godParent: null
			}
		},
		include: {
			godParent: {
				include: {
					groups: {
						where: {
							group: {
								visible: true
							}
						},
						include: {
							group: true
						}
					}
				}
			},
			groups: {
				where: {
					group: {
						visible: true
					}
				},
				include: {
					group: true
				}
			}
		}
	});

	const settings = await prisma.settings.findMany();

	return {
		me: locals.user,
		settings: settings,
		groups,
		aloneFirstYearUser,
		aloneSecondYearUser,
		relations
	};
};
