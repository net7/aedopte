import { prisma } from '$lib';
import { json } from '@sveltejs/kit';
import { z } from 'zod';

const groupSchema = z.object({
	id: z.string(),
	uid: z.string(),
	name: z.string(),
	pictureURL: z.string().nullable(),
	type: z.enum(['Club', 'Association', 'StudentAssociationSection'])
});

const groupObjectSchema = z.object({
	groupId: z.string(),
	userId: z.string(),
	group: groupSchema
});

// Définition du schéma Zod pour le modèle User
const UserSchema = z.object({
	id: z.string(), // Prisma génère une valeur par défaut pour l'ID
	churrosUid: z.string(), // Champ unique, donc on peut supposer qu'il est requis
	profilePicURL: z.string().nullable(), // Optionnel et doit être une URL valide si fourni
	givenName: z.string().default(''), // Valeur par défaut si non fourni
	surname: z.string().nullable(),
	description: z.string().nullable(),
	hobbies: z.string().nullable(),
	admin: z.boolean().default(false), // Valeur par défaut
	yearTier: z.number().gt(0).lt(3), // Doit être un entier entre 1 et 2
	apprentice: z.boolean().default(false), // Valeur par défaut
	major: z.enum(['sdn', 'eeea', 'mfee']),
	alreadyFindMate: z.boolean().default(false), // Valeur par défaut
	groups: groupObjectSchema.array().nullable() // Possible que le 1A ai mis aucun groupe
});

export async function POST({ request }) {
    const data = await request.json();
	console.info(data)

	if(data.action === 'deleteUser') {
		const userData = UserSchema.parse(data.user);

		try {
			await prisma.user.delete({
				where: {
					id: userData.id
				}
			})
		} catch (error) {
			return json({ 
				success: false,
				message: `Impossible de supprimer le compte : ${error}`
			});
		}

		return json({ success: true });
	} else if (data.action === "deleteRelation") {
		console.info("caca")

		const firstYearUserId = data.firstYearUserId;
		const secondYearUserId = data.secondYearUserId;

		try {
			await prisma.user.updateMany({ 
				where: {
					OR: [
						{ id: firstYearUserId as string },
						{ id: secondYearUserId as string }
					]
				},
				data: {
					alreadyFindMate: false,
					mateId: null,
				}
			})

		} catch (error) {
			console.info(error)
			return json({ 
				success: false,
				message: `Impossible de supprimer la relation : ${error}`
			});
		}
		
		return json({ success: true });

	} else if (data.action === "createRelation") {
		console.info("pipi")
		console.info(data)
		const firstYearUserId = data.firstYearUserId;
		const secondYearUserId = data.secondYearUserId;

		try {
			
			await prisma.user.update({
				where: {
					id: firstYearUserId as string
				},
				data: {
					alreadyFindMate: true,
					mateId: secondYearUserId as string
				}
			});
			
			await prisma.user.update({
				where: {
					id: secondYearUserId as string
				},
				data: {
					alreadyFindMate: true,
					mateId: firstYearUserId as string
				}
			})

		} catch (error) {
			console.info(error)
			return json({ 
				success: false,
				message: `Impossible de créer la relation : ${error}`
			});
		}

		return json({ success: true });
	} else {
		return json({
			success: false,
			message: "Action inconnue"
		})
	}
}