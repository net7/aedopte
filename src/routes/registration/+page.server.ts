import { prisma } from '$lib/server/prisma';
import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

//actualisation du user a la reception de la requete du form

export const load: PageServerLoad = async ({ locals }) => {
	const { user } = locals;

	// Page réservée aux utilisateurs en 1ere année
	if (!user) {
		redirect(302, '/login');
	}

	try {
		const me = await prisma.user.findUnique({
			where: {
				id: user.id
			}
		});
	
		const groups = await prisma.group.findMany({
			where: {
				visible: true
			}
		});
	
		const userGroups = await prisma.groupMember.findMany({
			where: {
				userId: user.id
			}
		});
		const userGroupsId = userGroups.map((userGroup) => {
			return userGroup.groupId;
		});
	
		const settings = await prisma.settings.findMany();
	
		return {
			me,
			settings: settings,
			groups,
			userGroupsId
		};
	} catch (error) {
		return {
			error: {
				message: `Erreur lors de la récupération des données: ${error}`
			}
		};
	}
};
