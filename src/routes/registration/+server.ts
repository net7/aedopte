import { prisma } from '$lib/server/prisma';
import { json } from '@sveltejs/kit';
import { z } from 'zod';

const groupSchema = z.object({
	id: z.string(),
	uid: z.string(),
	name: z.string(),
	pictureURL: z.string().nullable(),
	type: z.enum(['Club', 'Association', 'StudentAssociationSection'])
});

const MAX_FILE_SIZE = 5000000;
const ACCEPTED_IMAGE_TYPES = ["image/jpeg", "image/jpg", "image/png", "image/webp"];

const requestBodySchema = z.object({
	changeGroups: z.array(groupSchema),
	alreadyFindMate: z.string(),
	surname: z.string().nullable(),
	description: z.string().nullable(),
	hobbies: z.string().nullable(),
	godParentExpectation: z.string().nullable(),
	favoriteParty: z.string().nullable(),
	favoriteDrink: z.string().nullable(),
	favoriteMusic: z.string().nullable(),
	coinche: z.string().nullable(),
	hint: z.string().nullable(),
	base64ToUpload: z.string().nullable()
});

export async function POST({ request, locals }) {
	try {

		const data = await request.json();

		// Check si le type est bon
		const requestData = requestBodySchema.parse(data);

		const alreadyFindMate = requestData.alreadyFindMate === 'Oui';
		
		if (locals.user != null) {
		
			await prisma.user.update({
				where: {
					id: locals.user.id
				},
				data: {
					surname: requestData.surname,
					description: requestData.description,
					hobbies: requestData.hobbies,
					alreadyFindMate,
					favoriteParty: requestData.favoriteParty,
					godParentExpectation: requestData.godParentExpectation,
					favoriteDrink: requestData.favoriteDrink,
					favoriteMusic: requestData.favoriteMusic,
					coinche: requestData.coinche,
					hint: requestData.hint ?? '',
					hintPicture: requestData.base64ToUpload ?? ''
				}
			});

			//Supression des anciens liens des gens
			if (alreadyFindMate) {

				await prisma.linkedUsers.deleteMany({
					where: {
						childUserId: locals.user.id
					}
				})
			}
		
		
				const alreadyJoinGroup = await prisma.groupMember.findMany({
					where: {
						groupId: {
							in: requestData.changeGroups.map((group) => group.id)
						},
						userId: locals.user.id
					},
					select: {
						groupId: true
					}
				});
		
				// Check si l'utilisateur est déjà d'un des groupes cliqué et le supprime si c'est le cas, l'ajoute sinon
				for (const group of requestData.changeGroups) {
					if (alreadyJoinGroup.find((alreadyJoinGroup) => group.id === alreadyJoinGroup.groupId)) {
						await prisma.groupMember.delete({
							where: {
								groupId_userId: {
									groupId: group.id,
									userId: locals.user.id
								}
							}
						});
					} else {
						await prisma.groupMember.create({
							data: {
								group: {
									connect: {
										id: group.id
									}
								},
								user: {
									connect: {
										id: locals.user.id
									}
								}
							}
						});
					}
				}
			}
		
			return json({ success: true });

	} catch (error) {
		return json({ 
			success: false,
			message: `Erreur lors de la mise à jour du profil: ${error}`

		});
	}
}
