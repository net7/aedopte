import { OAuth2RequestError } from 'arctic';
import { error, text } from '@sveltejs/kit';
import { authentikOauth } from '$lib/server/auth';
import { prisma } from '$lib/server/prisma';
import { Major } from '@prisma/client';

import { lucia } from '$lib/server/auth';

export async function GET(event) {
	const code = event.url.searchParams.get('code');
	const state = event.url.searchParams.get('state');

	const storedState = event.cookies.get('oauthState');
	const codeVerifier = event.cookies.get('oauthCodeVerifier');
	// Validate Ouath state and code verifer
	if (!code || !state || !storedState || !codeVerifier || state !== storedState) {
		return text('Invalid state or code verifier', {
			status: 400
		});
	}

	// Possible petite dinguerie les lignes au dessus, jsp si la méthode validateAuthorizationCode fait les m^mes verif que au dessus...
	const token = await authentikOauth.validateAuthorizationCode(code, codeVerifier);
	try {
		const userInfo = await fetch('https://auth.inpt.fr/application/o/userinfo/', {
			method: 'post',
			headers: {
				Authorization: `Bearer ${token.accessToken}`,
				'Content-Type': 'application/json'
			}
		});
		const userInfoJ = await userInfo.json();
		if (userInfo) {
			let user = await prisma.user.findUnique({
				where: {
					churrosUid: userInfoJ.uid
				}
			});

			if (!user) {
				//On empêche les 3A de s'inscrire sur la plateforme
				if (userInfoJ.yearTier <= 2) {
					user = await prisma.user.create({
						data: {
							churrosUid: userInfoJ.uid,
							profilePicURL: userInfoJ.pictureURL,
							givenName: userInfoJ.given_name,
							yearTier: userInfoJ.yearTier,
							major: userInfoJ.major.uid as Major,
							apprentice: userInfoJ.apprentice,
							admin: userInfoJ.admin
						}
					});
				} else {
					throw error(401, "Vous n'êtes pas autorisé à vous créer de compte sur cette plateforme");
				}
			}
			//Création de la session pour l'utilisateur
			const session = await lucia.createSession(user.id, {
				userId: user.id,
				expiresAt: new Date(Date.now() + 1000 * 60 * 60 * 24 * 15) // 15 jours, soit la durée de vie initial d'un token avec la lib
			});
			const sessionCookie = lucia.createSessionCookie(session.id);
			event.cookies.set(sessionCookie.name, sessionCookie.value, {
				path: '/',
				...sessionCookie.attributes
			});
			// Redirection vers la page d'acceuil.
			return new Response(null, {
				status: 302,
				headers: {
					Location: '/'
				}
			});
		} else {
			//Si on a pas réussi à fetch la db churros pour recup les infos utilisateurs
			throw error(
				500,
				'Une erreur est survenue lors de la récupération des informations de l"utilisateur'
			);
		}
	} catch (e) {
		//Si erreur pendant le processus d'authentification
		if (e instanceof OAuth2RequestError) {
			console.error(e);
			throw error(500, 'Une erreur est survenue lors du processus d"authentification');
		} else {
			console.error(e);
			throw error(500, 'Une erreur est survenue');
		}
	}
}
