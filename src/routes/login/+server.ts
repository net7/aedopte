import { authentikOauth } from '$lib/server/auth';
import { redirect } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

import { generateCodeVerifier, generateState } from 'arctic';

// Name of the cookies used for Oauth process
const OAUTH_STATE_COOKIE = 'oauthState';
const OAUTH_CODE_VERIFIER_COOKIE = 'oauthCodeVerifier';

export const GET: RequestHandler = async ({ cookies }) => {
	// Generate a unique state value for the OAuth process
	const state = generateState();
	const codeVerifier = generateCodeVerifier();

	const url = await authentikOauth.createAuthorizationURL(state, codeVerifier, {
		scopes: ['openid', 'profile', 'email', 'churros:profile']
	});

	// store state as cookie
	cookies.set(OAUTH_STATE_COOKIE, state, {
		secure: false, // set to false in localhost
		path: '/',
		httpOnly: true,
		maxAge: 60 * 10 // 10 min
	});

	// strore codeVerifier as cookie
	cookies.set(OAUTH_CODE_VERIFIER_COOKIE, codeVerifier, {
		secure: false,
		path: '/',
		httpOnly: true,
		maxAge: 60 * 10
	});
	// Redirect the user to the Authentik OAuth authorization URL
	redirect(302, url);
};
