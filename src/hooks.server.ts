import { sequence } from '@sveltejs/kit/hooks';
import * as Sentry from '@sentry/sveltekit';
import { prisma } from '$lib/server/prisma';
import { lucia } from '$lib/server/auth';
import { redirect, type Handle } from '@sveltejs/kit';
import { env } from '$env/dynamic/public';

Sentry.init({
	dsn: env.PUBLIC_SENTRY_DSN,
    tracesSampleRate: 1,
});

export const handle: Handle = sequence(Sentry.sentryHandle(), async ({ event, resolve }) => {

		const sessionId = event.cookies.get(lucia.sessionCookieName);
		if (!sessionId) {
			event.locals.user = null; // A regler mais d'experience ça pose pas problème juste ts chouine
			event.locals.session = null;
			return resolve(event);
		}

		const { session, user } = await lucia.validateSession(sessionId);
		if (session && session.fresh) {
			const sessionCookie = lucia.createSessionCookie(session.id);
			event.cookies.set(sessionCookie.name, sessionCookie.value, {
				path: '.',
				...sessionCookie.attributes
			});
		}
		if (!session) {
			const sessionCookie = lucia.createBlankSessionCookie();
			event.cookies.set(sessionCookie.name, sessionCookie.value, {
				path: '.',
				...sessionCookie.attributes
			});
			throw redirect(302, '/login');
		}

		const userPrisma = await prisma.user.findUnique({
			where: { id: session.userId }
		});
		if (userPrisma) {
			event.locals.user = userPrisma;
		}
		event.locals.session = session;

		return await resolve(event);
});
export const handleError = Sentry.handleErrorWithSentry();
