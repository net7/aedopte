import { PrismaClient, relationType, Group, Major, User } from '@prisma/client';
import { fakerFR as faker } from '@faker-js/faker';

const prisma = new PrismaClient();

faker.seed(20);

//Nombre d'utilisateurs à créer pour le seeding.
const numberUser = 300;

const majors: Major[] = ['sdn', 'eeea', 'mfee'];

let users: User[] = [];

for (let i = 0; i < numberUser; i++) {
	const firstName = faker.person.firstName();
	const lastName = faker.person.lastName();
	const yearTier = faker.helpers.arrayElement([1, 1, 1, 2, 2]);

	users.push(
		await prisma.user.create({
			data: {
				givenName: `${firstName} ${lastName}`,
				admin: false,
				yearTier: yearTier,
				churrosUid: faker.string.nanoid(10), // Rien a foutre que ce soit pas le format d'un churrosUid
				surname: faker.datatype.boolean(0.3) ? faker.internet.userName() : null,
				major: faker.helpers.arrayElement(majors),
				apprentice: faker.datatype.boolean(0.1),

				//alreadyFindMate: faker.datatype.boolean(0.2),

				description: faker.lorem.paragraph(),
				hobbies: faker.lorem.paragraph(),
				hint: faker.lorem.paragraph()
			}
		})
	);
}

// Seul user qui est admin, (sauf possiblement un compte admin net7....)
await prisma.user.create({
	data: {
		churrosUid: 'versairea',
		givenName: 'Aurélien Versaire', //Merci copilot mdrrr
		admin: true,
		major: 'sdn',
		yearTier: 2
	}
});

const firstYearUsers = await prisma.user.findMany({
	where: {
		yearTier: 1
	}
});

const secondYearUsers = await prisma.user.findMany({
	where: {
		yearTier: 2
	}
});

//Selection de couple de 2A et 1A qui ont déjà trouvé un mate
const coupleWithoutAEdopte = faker.number.int({ min: 5, max: 8 });
const firstYearUserWithoutAEdopte = faker.helpers.arrayElements(
	firstYearUsers,
	coupleWithoutAEdopte
);
const secondYearUserWithoutAEdopte = faker.helpers.arrayElements(
	secondYearUsers,
	coupleWithoutAEdopte
);
for (let i = 0; i < coupleWithoutAEdopte; i++) {
	await prisma.user.update({
		where: {
			churrosUid: secondYearUserWithoutAEdopte[i].churrosUid
		},
		data: {
			alreadyFindMate: true,
			mateId: firstYearUserWithoutAEdopte[i].id
		}
	});

	await prisma.user.update({
		where: {
			churrosUid: firstYearUserWithoutAEdopte[i].churrosUid
		},
		data: {
			alreadyFindMate: true,
			mateId: secondYearUserWithoutAEdopte[i].id
		}
	});
}

const linkUsersType: relationType[] = ['LIKE', 'DISLIKE', 'MAYBE'];
const linkUsersTypeWithoutLike: relationType[] = ['DISLIKE', 'MAYBE'];

// Création des liens entre les utilisateurs
for (const secondYearUser of secondYearUsers) {
	const selectedFirstYearUsers = faker.helpers.arrayElements(
		firstYearUsers,
		faker.number.int({ min: 0, max: 30 })
	);
	let lovedRelation = 0;

	for (const firstYearUser of selectedFirstYearUsers) {
		let relation: relationType;
		if (lovedRelation < 5) {
			relation = faker.helpers.arrayElement(linkUsersType);
			if (relation === 'LIKE') {
				lovedRelation++;
			}
		} else {
			relation = faker.helpers.arrayElement(linkUsersTypeWithoutLike);
		}

		await prisma.linkedUsers.create({
			data: {
				parentUser: { connect: { id: secondYearUser.id } },
				childUser: { connect: { id: firstYearUser.id } },
				relation,
				rank: relation === 'LIKE' ? lovedRelation : null
			}
		});
	}
}

let groupsResquet = await fetch('https://churros.inpt.fr/graphql', {
	method: 'POST',
	mode: 'cors',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		query: `
                    {
                        groups {
                            name
                            uid
                            pictureURL
                            type
                        }
                    }
                 `
	})
}).then(async (r) => {
	const result = await r.json();
	return result;
});

let groups: Group[] = groupsResquet.data.groups;

//Filtre afin de ne selectionner que les clubs, asso et AE.
const selectedGroupType = ['Club', 'Association', 'StudentAssociationSection'];
groups = groups.filter((group) => selectedGroupType.includes(group.type));

await prisma.group.createMany({ data: groups });
// Attribution des groupes aux utilisateurs

for (let user of users) {
	const clubNumber = faker.number.int({ min: 0, max: 20 });
	const clubs = faker.helpers.arrayElements(groups, clubNumber);
	for (let i = 0; i <= clubNumber; i++) {
		await prisma.groupMember.create({
			data: {
				user: { connect: { id: user.id } },
				group: { connect: { uid: groups[i].uid } }
			}
		});
	}
} 

