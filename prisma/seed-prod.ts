import { PrismaClient, relationType, Group, Major, User } from '@prisma/client';

const prisma = new PrismaClient();

// init groups from churros
let groupsResquet = await fetch('https://churros.inpt.fr/graphql', {
	method: 'POST',
	mode: 'cors',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		query: `
                    {
                        groups {
                            name
                            uid
                            pictureURL
                            type
                        }
                    }
                 `
	})
}).then(async (r) => {
	const result = await r.json();
	return result;
});

let groups: Group[] = groupsResquet.data.groups;

//Filtre afin de ne selectionner que les clubs, asso et AE.
const selectedGroupType = ['Club', 'Association', 'StudentAssociationSection'];
groups = groups.filter((group) => selectedGroupType.includes(group.type));

await prisma.group.createMany({ data: groups });

// init settings
await prisma.settings.create({
	data: {
		key: 'showHint',
		value: 'false'
	}
});
