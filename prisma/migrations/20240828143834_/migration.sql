/*
  Warnings:

  - You are about to drop the column `imageUrl` on the `Group` table. All the data in the column will be lost.
  - Added the required column `pictureURL` to the `Group` table without a default value. This is not possible if the table is not empty.
  - Added the required column `type` to the `Group` table without a default value. This is not possible if the table is not empty.
  - Added the required column `uid` to the `Group` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Group" DROP COLUMN "imageUrl",
ADD COLUMN     "pictureURL" TEXT NOT NULL,
ADD COLUMN     "type" TEXT NOT NULL,
ADD COLUMN     "uid" TEXT NOT NULL;
