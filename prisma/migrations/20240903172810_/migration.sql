/*
  Warnings:

  - A unique constraint covering the columns `[mateId]` on the table `User` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "User_mateId_key" ON "User"("mateId");

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_mateId_fkey" FOREIGN KEY ("mateId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
