/*
  Warnings:

  - You are about to drop the `clubs` table. If the table is not empty, all the data it contains will be lost.

*/
-- CreateEnum
CREATE TYPE "relationType" AS ENUM ('LIKE', 'DISLIKE', 'MAYBE');

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "alreadyFindGodchild" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "alreadyFindGodparent" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "description" TEXT,
ADD COLUMN     "hobbies" TEXT,
ADD COLUMN     "surname" TEXT;

-- DropTable
DROP TABLE "clubs";

-- CreateTable
CREATE TABLE "LinkedUsers" (
    "parentUserId" TEXT NOT NULL,
    "childUserId" TEXT NOT NULL,
    "relation" "relationType" NOT NULL,

    CONSTRAINT "LinkedUsers_pkey" PRIMARY KEY ("parentUserId","childUserId")
);

-- CreateTable
CREATE TABLE "UserInterestedByGroup" (
    "groupId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "UserInterestedByGroup_pkey" PRIMARY KEY ("groupId","userId")
);

-- CreateTable
CREATE TABLE "LovedUsers" (
    "id" TEXT NOT NULL DEFAULT nanoid(),
    "userId" TEXT NOT NULL,
    "ranking" INTEGER,

    CONSTRAINT "LovedUsers_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Group" (
    "id" TEXT NOT NULL DEFAULT nanoid(),
    "name" TEXT NOT NULL,
    "imageUrl" TEXT NOT NULL,

    CONSTRAINT "Group_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "LinkedUsers" ADD CONSTRAINT "LinkedUsers_parentUserId_fkey" FOREIGN KEY ("parentUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "LinkedUsers" ADD CONSTRAINT "LinkedUsers_childUserId_fkey" FOREIGN KEY ("childUserId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserInterestedByGroup" ADD CONSTRAINT "UserInterestedByGroup_groupId_fkey" FOREIGN KEY ("groupId") REFERENCES "Group"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserInterestedByGroup" ADD CONSTRAINT "UserInterestedByGroup_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "LovedUsers" ADD CONSTRAINT "LovedUsers_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
