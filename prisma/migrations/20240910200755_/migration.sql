/*
  Warnings:

  - You are about to drop the column `godParentExpectations` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `sport` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "godParentExpectations",
DROP COLUMN "sport",
ADD COLUMN     "godParentExpectation" TEXT;
