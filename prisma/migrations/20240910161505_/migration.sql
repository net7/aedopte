-- AlterTable
ALTER TABLE "User" ADD COLUMN     "coinche" TEXT,
ADD COLUMN     "favoriteDrink" TEXT,
ADD COLUMN     "favoriteMusic" TEXT,
ADD COLUMN     "favoriteParty" TEXT,
ADD COLUMN     "godParentExpectations" TEXT,
ADD COLUMN     "sport" TEXT;
