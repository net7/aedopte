/*
  Warnings:

  - The values [SN,EEEA,MFEE] on the enum `Major` will be removed. If these variants are still used in the database, this will fail.
  - Made the column `major` on table `User` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "Major_new" AS ENUM ('sdn', 'eeea', 'mfee');
ALTER TABLE "User" ALTER COLUMN "major" TYPE "Major_new" USING ("major"::text::"Major_new");
ALTER TYPE "Major" RENAME TO "Major_old";
ALTER TYPE "Major_new" RENAME TO "Major";
DROP TYPE "Major_old";
COMMIT;

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "apprentice" BOOLEAN NOT NULL DEFAULT false,
ALTER COLUMN "major" SET NOT NULL;
