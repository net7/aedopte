-- CreateEnum
CREATE TYPE "Major" AS ENUM ('SN', 'EEEA', 'MFEE');

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "major" "Major";
