/*
  Warnings:

  - You are about to drop the `UserInterestedByGroup` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "UserInterestedByGroup" DROP CONSTRAINT "UserInterestedByGroup_groupId_fkey";

-- DropForeignKey
ALTER TABLE "UserInterestedByGroup" DROP CONSTRAINT "UserInterestedByGroup_userId_fkey";

-- DropTable
DROP TABLE "UserInterestedByGroup";

-- CreateTable
CREATE TABLE "groupMember" (
    "groupId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "groupMember_pkey" PRIMARY KEY ("groupId","userId")
);

-- AddForeignKey
ALTER TABLE "groupMember" ADD CONSTRAINT "groupMember_groupId_fkey" FOREIGN KEY ("groupId") REFERENCES "Group"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "groupMember" ADD CONSTRAINT "groupMember_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
