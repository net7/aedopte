/*
  Warnings:

  - You are about to drop the `LovedUsers` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "LovedUsers" DROP CONSTRAINT "LovedUsers_userId_fkey";

-- AlterTable
ALTER TABLE "LinkedUsers" ADD COLUMN     "rank" INTEGER;

-- DropTable
DROP TABLE "LovedUsers";
