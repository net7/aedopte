-- DropForeignKey
ALTER TABLE "LinkedUsers" DROP CONSTRAINT "LinkedUsers_childUserId_fkey";

-- DropForeignKey
ALTER TABLE "LinkedUsers" DROP CONSTRAINT "LinkedUsers_parentUserId_fkey";

-- AddForeignKey
ALTER TABLE "LinkedUsers" ADD CONSTRAINT "LinkedUsers_childUserId_fkey" FOREIGN KEY ("childUserId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "LinkedUsers" ADD CONSTRAINT "LinkedUsers_parentUserId_fkey" FOREIGN KEY ("parentUserId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
