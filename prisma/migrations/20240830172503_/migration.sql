/*
  Warnings:

  - You are about to drop the column `alreadyFindGodchild` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `alreadyFindGodparent` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "alreadyFindGodchild",
DROP COLUMN "alreadyFindGodparent",
ADD COLUMN     "alreadyFindMate" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "profilePicURL" TEXT;
