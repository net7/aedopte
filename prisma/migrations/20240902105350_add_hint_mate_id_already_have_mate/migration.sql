-- AlterTable
ALTER TABLE "User" ADD COLUMN     "alreadyHaveMate" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "hint" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "mateId" TEXT;
